import { Container, Divider, MantineProvider, Stack, Text } from '@mantine/core';
import { Notifications } from '@mantine/notifications';
import NewTaskComponent from './components/new-task.component';
import TaskListComponent from './components/task-list.component';
export default function App() {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <Notifications />

      <Container size="xs" px="xs"  style={{marginTop: "5px"}}>
        <Stack h={300} sx={(theme) => ({ backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0] })}>
        
          <NewTaskComponent />
          <Divider /> 
          <TaskListComponent />
      </Stack>

      </Container>

    </MantineProvider>
  );
}