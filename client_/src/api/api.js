import axios from 'axios';

const API = axios.create({
    baseURL: `${process.env.REACT_APP_API}/api`
})


export const getTasks = async () => await API.get('/tasks');
export const createTask = async (task) => await API.post('/tasks', task);
export const deleteTask = async (id) => await API.delete(`/tasks/${id}`); 