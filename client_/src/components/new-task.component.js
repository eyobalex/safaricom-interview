

import { Button, Group, TextInput } from '@mantine/core'
import { notifications } from '@mantine/notifications';
import React, { useEffect, useState } from 'react'
import { createTaskService } from '../services/task.services';
import { useTaskState } from '../store/task.store';

const NewTaskComponent = () => {
    const [name, setName] = useState('');

    
    const addTask = useTaskState((state) => state.addTask)

    const handleAdd =async () => {

        try{
            if(!name) {
                notifications.show({
                    title: "Error",
                    message: "Name shouldn't be empty"
                });
                return;
            }
            const task = await createTaskService({name: name});
    
            addTask(task.data)
            notifications.show({
                title: "Task Created",
                message: "Task created Successfully"
            });
        }catch(error){
            notifications.show({
                title: "Error",
                message: error.message
            });

        }
       
    }
  return (
    <div>

        <Group position='apart'>
            <TextInput value={name} onChange={(e) => setName(e.target.value)} /> 
            <Button onClick={handleAdd} > Add </Button>
        </Group>
        
    </div>
  )
}

export default NewTaskComponent