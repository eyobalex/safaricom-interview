import { Button, Group, List } from "@mantine/core";
import { notifications } from "@mantine/notifications";
import { useEffect } from "react";
import { deleteTaskService, getTasksService } from "../services/task.services";
import { useTaskState } from "../store/task.store"


export default function(props){

    const tasks = useTaskState((state) => (state.tasks));
    const deleteTask = useTaskState((state) => (state.deleteTask))

    useEffect( () => {

        (async() =>{
            await getTasksService();
        })()
    }, [])

    const handleDelete = async (id) => {

        try {

           await deleteTask(id);
           await deleteTaskService(id);

           notifications.show({
            title: "Deleted Successfully",
            message: "Task deleted successfully"
           })
            
        } catch (error) {
            console.log(error.message);
        }

    }


    return (
        <>

        {tasks ? (
            <List>
                {tasks.map((task, i) => (
                    <List.Item key={i}>
                        <Group position="apart">
                        Name : { task.name}
                                <Button onClick={() => handleDelete(task.id)}>Delete</Button>
                        </Group>
                    </List.Item>
                ))}
               
          </List>
        ): (
            <List>
                <List.Item>There are no tasks available</List.Item>
      </List>
        )}
        
        </>
    )
}