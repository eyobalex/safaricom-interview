
import {createTask, deleteTask, getTasks} from '../api/api.js'

import { notifications } from '@mantine/notifications';


/**
 * 
 * @param {task} 
 * @returns promise of the created task
 */
export const createTaskService = async (task) => {

    try{
        return await createTask(task);
    }catch(error) {
        notifications.show({
            title: 'Task',
            color: 'red',
            message: error.message,
          })
    }
}
/**
 * 
 * @returns a collection of tasks
 */
export const getTasksService = async () => {

    try{
         return await getTasks();
    }catch(error) {
        notifications.show({
            title: 'Task ',
            color: 'red',
            message: error.message,
          })
    }
}

/**
 * 
 * @param  id id of the task
 */
export const deleteTaskService = async (id) => {

    try{
         return await deleteTask(id);
    }catch(error) {
        notifications.show({
            title: 'Task ',
            color: 'red',
            message: error.message,
          })
    }
}