import {create} from 'zustand'


export const useTaskState = create((set) => ({
    tasks: [],
    addTask: (task) => set((store) => ([...store.tasks, task])),
    removeTask: (id) => set(store => store.tasks.filter(task => task.id !== id))
}));