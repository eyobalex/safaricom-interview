import express from 'express';
import { connect } from './config/db.js';
import taskRouter from './routes/task.route.js';
import cors from 'cors';
import dotenv from 'dotenv'
dotenv.config()
const app = express();


app.use(express.json())
app.use(cors)


app.get('/', (req, res) => {
    return res.status(200).json({message: "Safaricom PWA API"})
})

app.use('/app/tasks', taskRouter)

connect().then(()=> {
    app.listen(process.env.PORT || 5000, () =>  {
        console.log(`Server started on http://localhost:${process.env.PORT || 5000}`);
    })
}).catch((err) => {
    console.log(err);
    console.error("Unable to connect to the database");
})