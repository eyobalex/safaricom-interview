import mongoose from "mongoose";
import { MongoMemoryServer } from 'mongodb-memory-server'

/**
 * creates a mongoose connection object using mongodb memory server
 * 
 * @returns a promise of mongoose connection object
 */
export async function connect() {

    // const mongodb = await MongoMemoryServer.create()

    // const dbUri = mongodb.getUri()

    const dbUri = process.env.MONGODB_URL;

    const db = mongoose.connect(dbUri);

    return db;
}