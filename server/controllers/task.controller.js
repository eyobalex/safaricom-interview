import {createTask, deleteTask, updateTask, getTasks} from '../services/task.service.js'
/**
 *
 * 
 * @returns json response of a collection of Tasks
 */
export const getTasksController = async (req, res) => {

    try {
        const tasks = await getTasks()
        return res.status(200).json({data: task});
    } catch (error) {
        return res.status(500).json({message: "Unable to process your request!"});
        
    }
}



/**
 *
 * @body {name: string, required}
 * 
 * @returns json response
 */
export const createTaskController = async (req, res) => {

    try {

        const {name} = req.body;
        if(!name) return res.status(400).json({message: "Name should be provided!"});

        const task = await createTask(req.body)
        return res.status(201).json({message: "Task created successfully!", data: task});
    } catch (error) {

        return res.status(500).json({message: "Unable to process your request!"});
        
    }
}



/**
 *
 * @param id : required
 * @body {
 * name: string| required,
 * completed: boolean| optional
 * }
 * 
 * @returns json response 
 */
export const deleteTaskController = async (req, res) => {

    try {
        const {id} = req.params;
        if(!id) return res.status(400).json({message: "Id should be provided!"});

        await deleteTask(id)
        return res.status(200).json({message: "Task deleted successfully!"});
    } catch (error) {
        return res.status(500).json({message: "Unable to process your request!"});
    
    }
}

/**
 *
 * @body {
 * name: string| required,
 * completed: boolean| optional
 * }
 * 
 * @returns json response 
 */
export const updateTaskController = async (req, res) => {

    try {

        const {name} = req.body;
        if(!name) return res.status(400).json({message: "Name should be provided!"});

        const task = await updateTask(req.body)
        return res.status(200).json({message: "Task updated successfully!"});
    } catch (error) {
        return res.status(500).json({message: "Unable to process your request!"});
    
    }
}