import mongoose from "mongoose";

const TaskSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Task should have a name"],
        
    },
    completed: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('Task', TaskSchema);