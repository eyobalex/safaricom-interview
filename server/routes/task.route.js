import { Router } from "express"
import { createTaskController, deleteTaskController, getTasksController, updateTaskController } from "../controllers/task.controller.js"


const router = Router()

router.get('/', getTasksController);
router.post('/', createTaskController);
router.put('/:id', updateTaskController);
router.delete('/:id', deleteTaskController)

export default router;