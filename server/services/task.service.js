import TaskModel from "../models/Task.model.js";


export const createTask = async (task) => await TaskModel.create(task);

export const updateTask = async (id, task) => await TaskModel.findByIdAndUpdate(id, task);

export const getTasks = async () => await TaskModel.find({});

export const deleteTask = async (id) => await TaskModel.findByIdAndDelete(id)